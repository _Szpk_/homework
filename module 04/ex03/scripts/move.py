#!/usr/bin/env python
import rospy 
import sys
from geometry_msgs.msg  import Twist
from sensor_msgs.msg  import LaserScan
import time
        
sector = 0

def callback(data):
	global sector 
	ranges = list(data.ranges)
	for i in range(720):
		if (ranges[i] >30):
			ranges[i] = 30
	sector = (ranges[359]+ranges[360]+ranges[361])/3
	
	

if __name__ == '__main__':
    try:
	#Creating our node,publisher and subscriber

	rospy.init_node('move_forward', anonymous=True)
	velocity_publisher = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
	rate = rospy.Rate(10)
	vel_msg = Twist()

	while not rospy.is_shutdown():
	     
	     lidar = rospy.Subscriber('/rrbot/laser/scan',LaserScan,callback)
	     if (sector > 1.3):
		     print("forward, dist:",sector)
	             vel_msg.linear.x = 0.1
	             vel_msg.linear.y = 0
	             vel_msg.linear.z = 0
	             vel_msg.angular.x = 0
	             vel_msg.angular.y = 0
	             vel_msg.angular.z = 0

	     else:
		     print("stop, dist: ",sector)
		     vel_msg.linear.x = 0
	             vel_msg.linear.y = 0
	             vel_msg.linear.z = 0
	             vel_msg.angular.x = 0
	             vel_msg.angular.y = 0
	             vel_msg.angular.z = 0
	     #Publishing our vel_msg
             velocity_publisher.publish(vel_msg)
             rate.sleep()
    except rospy.ROSInterruptException: pass
