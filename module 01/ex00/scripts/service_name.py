#!/usr/bin/env python

from __future__ import print_function

from service_full_name.srv import service_name, service_nameResponse
import rospy

def handle_full_name(req):
    print("Returning [%s + %s + %s = %s]"%(req.name1, req.name2, req.name3, (req.name1 +" "+ req.name2 +" " + req.name3)))
    return service_nameResponse(req.name1+" " + req.name2+" " + req.name3)


def full_name_server():
    rospy.init_node('full_name_server')
    s = rospy.Service('summ_full_name', service_name, handle_full_name)
    print("ready...")
    rospy.spin()


if __name__ == '__main__':
    full_name_server()
