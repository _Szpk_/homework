#!/usr/bin/env python

from __future__ import print_function

import sys
import rospy
from service_full_name.srv import *

def full_name_client(name1, name2, name3):
    rospy.wait_for_service('summ_full_name')
    try:
        service_full_name = rospy.ServiceProxy('summ_full_name', service_name)
        resp1 = service_full_name(name1, name2, name3)
        return resp1.full_name
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e) 

def usage():
    return "%s [x y]"%sys.argv[0]

if __name__ == "__main__":
    if len(sys.argv) == 4:
        name1 = str(sys.argv[1])
        name2 = str(sys.argv[2])
        name3 = str(sys.argv[3])
    else:
        print(usage())
        sys.exit(1)
    print('1 sec...')
    #print("Requesting %s+%s"%(x, y))
    print(full_name_client(name1, name2, name3))
    print('done!')
