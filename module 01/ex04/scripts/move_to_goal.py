#! /usr/bin/env python 

from cmath import sqrt
from math import atan2
import sys
import rospy 
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose

class Ex04:

    def __init__(self):
        rospy.init_node('turtle_move')
        self.vel = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
        self.pose_subscriber = rospy.Subscriber('/turtle1/pose', Pose, self.callback)
        self.pose = Pose()
        self.r = rospy.Rate(10)
        self.tolerance = 0.0001

    def callback(self, data):
        self.pose = data
        self.pose.x = round(self.pose.x, 8)
        self.pose.y = round(self.pose.y, 8)

    def distance(self, x, y):
        return sqrt((x-self.pose.x)**2+(y-self.pose.y)**2).real

    def turtle_target(self, x, y, alpha):
        
        vel = Twist()
        print(type(self.distance(self.pose.x, self.pose.y)))
        while (self.distance(x, y) >= self.tolerance):
            vel.linear.x = self.distance(x, y)
            vel.linear.y = 0
            vel.linear.z = 0
            vel.angular.x = 0
            vel.angular.y = 0
            vel.angular.z = 4 * (atan2(y - self.pose.y, x - self.pose.x) - self.pose.theta)
            print(self.pose.x, self.pose.y, self.pose.theta)
            self.vel.publish(vel)
            self.r.sleep()
        vel.linear.x = 0
        vel.linear.y = 0
        vel.angular.z = 0 


if __name__ == '__main__':
    print('lets go!')
    ex = Ex04()
    ex.turtle_target(float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]))