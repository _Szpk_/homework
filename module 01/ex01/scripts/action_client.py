#! /usr/bin/env python 
from __future__ import print_function
import sys

from numpy import angle

import rospy
import actionlib

from action_turtle_commands.msg import execute_turtle_commandsAction, execute_turtle_commandsGoal

def action_feedback(fb):
    print(fb)

def action_client():
    print('don')
    client = actionlib.SimpleActionClient('action_turtle_command', execute_turtle_commandsAction)
    client.wait_for_server()

    goal = execute_turtle_commandsGoal(command= 'forward', s=2, angle = 0)
    client.send_goal(goal, feedback_cb=action_feedback)
    client.wait_for_result()

    goal = execute_turtle_commandsGoal(command= 'turn_right', s=0, angle = 90)
    client.send_goal(goal, feedback_cb=action_feedback)
    client.wait_for_result()
    
    goal = execute_turtle_commandsGoal(command= 'forward', s=1, angle = 0)
    client.send_goal(goal, feedback_cb=action_feedback)
    client.wait_for_result()


    return client.get_result()

if __name__ == '__main__':
    try:
        rospy.init_node('action_client')
        result = action_client()
        print("Result:", result)
    except rospy.ROSInterruptException:
        print("program interrupted before completion", file=sys.stderr)